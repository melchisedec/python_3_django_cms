from django.contrib import admin
from .models import Post, Category, Comment
# Register your models here.

class CategoryAdmin( admin.ModelAdmin ):
	list_display = ( 'name', 'slug' )
	prepopulate_fields = { 'slug' : ( 'name', ) }

admin.site.register( Category, CategoryAdmin )

class PostAdmin( admin.ModelAdmin ):
	list_display = ( 'title', 'category', 'author', 'published', 'status' )
	list_filter = ( 'status', 'created', 'published', 'author' )
	search_fields = ( 'title', 'content' )
	prepopulate_fields = { 'slug' : ( 'title', ) }

admin.site.register( Post, PostAdmin )

class CommnentAdmin( admin.ModelAdmin ):
	list_display = ( 'user', 'email', 'approved' )

admin.site.register( Comment, CommnentAdmin )

